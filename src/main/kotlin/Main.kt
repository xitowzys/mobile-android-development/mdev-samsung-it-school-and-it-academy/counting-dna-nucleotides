fun main() {
    var A = 0
    var C = 0
    var G = 0
    var T = 0

    var str: String = readLine().toString()

    for (item: Char in str) {
        when (item) {
            'A' -> A++
            'C' -> C++
            'G' -> G++
            'T' -> T++
        }
    }

    println("$A $T $G $C")
}